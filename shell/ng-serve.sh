#!/bin/bash

# Exit on first error, and using a veriable before defining it is an error.
set -eu

# Move to the project root.
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"  
cd "$SCRIPT_DIR/.."

# Build shell.
docker-compose -f shell/docker-compose.yml build shell

# Run shell.
docker-compose -f shell/docker-compose.yml run -p 4200:4200 --rm shell