import { Component, OnInit } from '@angular/core';
import { ListItemsService } from '../list-items.service';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.css']
})
export class ListItemsComponent implements OnInit {
  itemsList: String[] = [];

  constructor(private listItemsService: ListItemsService) { }

  ngOnInit(): void {
    this.listItemsService.getItems().subscribe((items: String[])  => this.itemsList = items)
  }

}
